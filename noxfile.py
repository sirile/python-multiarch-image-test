import nox


@nox.session(python=False)
def tests(session):
    session.run(
        "coverage",
        "run",
        "--source=yinyang",
        "-m",
        "pytest",
        "--junitxml=report.xml",
    )
    session.run("coverage", "report")
    session.run("coverage", "xml")
    session.run("coverage", "html")
