import logging
import socket
import sys

from colorhash import ColorHash
from fastapi import FastAPI, HTTPException
from fastapi.responses import HTMLResponse

_logger = logging.getLogger(__name__)
hostname = socket.gethostname()
colour = ColorHash(hostname).hex
app = FastAPI()

# Constants for configuration and templates
LOG_LEVEL = logging.DEBUG
HTML_TEMPLATE = """
<!DOCTYPE html>
<html>
    <head><title>Python scaling demo</title></head>
    <body>
        <h1>{}</h1>
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 400 400">
            <circle cx="50" cy="50" r="48" fill="{}" stroke="#000"/>
            <path d="M50,2a48,48 0 1 1 0,96a24 24 0 1 1 0-48a24 24 0 1 0 0-48" fill="#000"/>
            <circle cx="50" cy="26" r="6" fill="#000"/>
            <circle cx="50" cy="74" r="6" fill="#FFF"/>
        </svg>
    </body>
</html>
"""


def setup_logging(level: int) -> None:
    """
    Configure the logging system with a specified log level.

    This function sets up basic logging configuration with a custom format
    that includes timestamp, log level, logger name, and the log message.
    The log output is directed to sys.stderr.

    Args:
        level (int): The desired logging level (e.g., logging.INFO, logging.DEBUG).

    Returns:
        None
    """
    logformat = "[%(asctime)s] %(levelname)s:%(name)s:%(message)s"
    logging.basicConfig(
        level=level,
        stream=sys.stderr,
        format=logformat,
    )


def get_yin_yang_svg() -> str:
    """
    Generate the HTML content for a Yin-Yang SVG image.

    Args:
        colour (str): The color to be used for the Yin part of the symbol.

    Returns:
        str: The HTML content of the SVG image.
    """
    return HTML_TEMPLATE.format(hostname, colour)


@app.get("/", response_class=HTMLResponse)
async def yingyang():
    """
    Generate and serve an HTML page with a Yin-Yang SVG image.

    This endpoint creates a simple HTML page containing:
    1. A title "Python scaling demo"
    2. An h1 header with the current hostname
    3. An SVG image of a Yin-Yang symbol

    The color of the Yin (light) part of the symbol is determined by the 'colour' variable.
    The hostname and color are logged at debug level.

    Returns:
        HTMLResponse: An HTML page with the Yin-Yang SVG image.
    """
    try:
        svg_content = get_yin_yang_svg()
        _logger.debug("Served image, host: %s, colour: %s", hostname, colour)
        return HTMLResponse(content=svg_content)
    except Exception as e:
        _logger.error("Error serving image: %s, Exception: %s", hostname, e)
        raise HTTPException(status_code=500, detail="Internal Server Error")


setup_logging(LOG_LEVEL)
_logger.info("Serving image, host: %s, colour: %s", hostname, colour)
