from fastapi.testclient import TestClient

from yinyang.main import app

client = TestClient(app)


def test_get_root():
    # This test checks if the root endpoint returns a successful status code (200)
    response = client.get("/")
    assert response.status_code == 200


def test_exception_handling(mocker):
    """
    This test verifies that the application correctly handles exceptions
    by returning an appropriate HTTP status code and error message.
    The test mocks the 'get_yin_yang_svg' function to cause an exception,
    then checks if the root endpoint returns a 500 Internal Server Error
    with the expected error message "Internal Server Error".
    """
    # Mocking the get_yin_yang_svg function to simulate an exception
    mocker.patch("yinyang.main.get_yin_yang_svg", side_effect=Exception)

    # Sending a GET request to the root endpoint
    response = client.get("/")

    # Asserting that the status code is 500 (Internal Server Error)
    assert response.status_code == 500

    # Asserting that the error message in the JSON response matches the expected message
    assert response.json()["detail"] == "Internal Server Error"
