FROM python:3.13-alpine as build
COPY dist dist
RUN pip install dist/*.whl

FROM python:3.13-alpine
COPY --from=build /usr/local/bin/hypercorn /usr/local/bin/hypercorn
COPY --from=build /usr/local/lib/python3.13/site-packages/ /usr/local/lib/python3.13/site-packages/
CMD ["hypercorn","--bind", "0.0.0.0:8000", "/usr/local/lib/python3.12/site-packages/yinyang/main:app"]
